import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello, Lockscreen Overlay!")

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("clicked, exiting...")
            Qt.quit()
        }
    }
}
