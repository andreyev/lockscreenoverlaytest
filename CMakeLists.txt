cmake_minimum_required(VERSION 3.16)

project(lockscreen-overlay-test)
set(PROJECT_VERSION "22.04")

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(QT_MIN_VERSION "6.5")
set(KF_MIN_VERSION "5.240.0")

find_package(ECM ${KF_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED Core Quick Qml DBus Widgets)
find_package(KF6 ${KF_MIN_VERSION} REQUIRED COMPONENTS CoreAddons I18n WindowSystem)
find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED WaylandClient)
find_package(Wayland REQUIRED COMPONENTS Client)
find_package(PlasmaWaylandProtocols 1.8 CONFIG)
set_package_properties(PlasmaWaylandProtocols PROPERTIES
    TYPE REQUIRED
    PURPOSE "Collection of Plasma-specific Wayland protocols"
    URL "https://invent.kde.org/libraries/plasma-wayland-protocols/"
)
find_package(QtWaylandScanner)
set_package_properties(QtWaylandScanner PROPERTIES
    TYPE REQUIRED
    PURPOSE "Required for building with Wayland above-lock-screen support"
)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_executable(lockscreenoverlaytest
  main.cpp
  qml.qrc
)

target_link_libraries(lockscreenoverlaytest
    Qt::Core
    Qt::Quick
    Qt::Qml
    Qt::DBus
    Qt::Widgets
    Qt::WaylandClient
    Qt::WaylandClientPrivate
    Wayland::Client
    KF6::WindowSystem
)

ecm_add_qtwayland_client_protocol(lockscreenoverlaytest
    PROTOCOL ${PLASMA_WAYLAND_PROTOCOLS_DIR}/kde-lockscreen-overlay-v1.xml
    BASENAME kde-lockscreen-overlay-v1
)
