// SPDX-FileCopyrightText: 2015 Marco Martin <mart@kde.org>
// SPDX-FileCopyrightText: 2021 Alexey Andreyev <aa13q@ya.ru>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QGuiApplication>

#include <QObject>
#include <QtQml>

#include <KWaylandExtras>
#include <KWindowSystem>

#include "qwayland-kde-lockscreen-overlay-v1.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>
#include <QQuickWindow>
#include <QWaylandClientExtensionTemplate>
#include <qpa/qplatformnativeinterface.h>

struct ScreenSaverUtils {
    Q_GADGET
public:
    Q_INVOKABLE static bool getActive()
    {
        bool active = false;
        QDBusMessage request = QDBusMessage::createMethodCall(QStringLiteral("org.freedesktop.ScreenSaver"),
                                                              QStringLiteral("/ScreenSaver"),
                                                              QStringLiteral("org.freedesktop.ScreenSaver"),
                                                              QStringLiteral("GetActive"));
        const QDBusReply<bool> response = QDBusConnection::sessionBus().call(request);
        active = response.isValid() ? response.value() : false;
        return active;
    }
};
Q_DECLARE_METATYPE(ScreenSaverUtils)

class WaylandAboveLockscreen : public QWaylandClientExtensionTemplate<WaylandAboveLockscreen>, public QtWayland::kde_lockscreen_overlay_v1
{
public:
    WaylandAboveLockscreen()
        : QWaylandClientExtensionTemplate<WaylandAboveLockscreen>(1)
    {
        initialize();
    }

    void allowWindow(QWindow *window)
    {
        QPlatformNativeInterface *native = qGuiApp->platformNativeInterface();
        wl_surface *surface = reinterpret_cast<wl_surface *>(native->nativeResourceForWindow(QByteArrayLiteral("surface"), window));

        Q_ASSERT(surface);
        allow(surface);
    }
};

static void raiseAboveLockscreen(QWindow *window)
{
    bool screenLocked = ScreenSaverUtils::getActive();
    if (screenLocked) {
        window->setVisibility(QWindow::Visibility::FullScreen);
        KWaylandExtras::requestXdgActivationToken(window, 0, QStringLiteral("org.kde.phone.dialer.desktop"));
        QObject::connect(KWaylandExtras::self(), &KWaylandExtras::xdgActivationTokenArrived, window, [window](int, const QString &token) {
            KWindowSystem::setCurrentXdgActivationToken(token);
            KWindowSystem::activateWindow(window);
        });
    }
}


int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    ScreenSaverUtils screenSaverUtils;
    engine.rootContext()->setContextProperty(QStringLiteral("ScreenSaverUtils"), QVariant::fromValue<ScreenSaverUtils>(screenSaverUtils));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    Q_ASSERT(KWindowSystem::isPlatformWayland());

    QWindow *window = qobject_cast<QWindow *>(engine.rootObjects().at(0));
    Q_ASSERT(window);

    Q_ASSERT(!window->isVisible());
    WaylandAboveLockscreen aboveLockscreen;
    Q_ASSERT(aboveLockscreen.isInitialized());
    aboveLockscreen.allowWindow(window);
    raiseAboveLockscreen(window);

    return app.exec();
}

#include "main.moc"
